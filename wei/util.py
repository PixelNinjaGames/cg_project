import numpy as np


def is_intersect(p0, p1, resolution=128):
    scale0 = p0.scale
    scale1 = p1.scale
    translation0 = p0.translation
    translation1 = p1.translation

    if scale0 > scale1:
        v0 = p0.to_voxel(
            size=resolution * 2, scale=resolution / 2 * scale1 / scale0,
            translation=None)
        v1 = p1.to_voxel(
            size=resolution * 2, scale=resolution / 2,
            translation=(translation1 - translation0) * scale1)
    else:
        v0 = p0.to_voxel(
            size=resolution * 2, scale=resolution / 2,
            translation=None)
        v1 = p1.to_voxel(
            size=resolution * 2, scale=resolution / 2 * scale0 / scale1,
            translation=(translation1 - translation0) * scale1)

    return np.max(v1 * v0) > 0
