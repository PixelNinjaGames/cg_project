from obj_parser import Parser
import vtk
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


parser = Parser('./last_examples/ChairFancy2/fancyChair2.obj')
parser.normalize()
parser.display()
[top, left, front] = parser.get_projections(256)

image_append = vtk.vtkImageAppend()
image_append.SetInputData(top)
image_append.AddInputData(left)
image_append.AddInputData(front)

viewer = vtk.vtkImageViewer()
interator = vtk.vtkRenderWindowInteractor()
viewer.SetInputConnection(image_append.GetOutputPort())
viewer.SetupInteractor(interator)
viewer.SetColorLevel(0)
viewer.Render()
interator.Start()

# voxel = parser.to_voxel(size=64)
# fig = plt.figure()
# fig.canvas.set_window_title('Display Voxel')
# fig.suptitle('Display Voxel', fontsize=12)
# ax = Axes3D(fig)
# ax.view_init(elev=60, azim=-30)
# ax.voxels(voxel, edgecolor='k')
# plt.show()
