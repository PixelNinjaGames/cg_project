import vtk

def loadObj(fname):
    """Load the given STL file, and return a vtkPolyData object for it."""
    reader = vtk.vtkOBJReader()
    reader.SetFileName(fname)
    reader.Update()
    polydata = reader.GetOutput()
    return polydata

originalBack = vtk.vtkPolyData()
originalSeat = vtk.vtkPolyData()
originalLegFrontLeft = vtk.vtkPolyData()
originalLegFrontRight = vtk.vtkPolyData()
originalLegBackLeft = vtk.vtkPolyData()
originalLegBackRight = vtk.vtkPolyData()
originalLeftArmrest = vtk.vtkPolyData()
originalRightArmrest = vtk.vtkPolyData()
originalLeftBar = vtk.vtkPolyData()
originalRightBar = vtk.vtkPolyData()

anotherBack = vtk.vtkPolyData()
anotherBack2 = vtk.vtkPolyData()

stlFilename = "last_examples/ChairFancy2/meshes/Back.obj"
originalBack.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/Seat.obj"
originalSeat.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/LegFrontLeft.obj"
originalLegFrontLeft.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/LegFrontRight.obj"
originalLegFrontRight.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/LegBackLeft.obj"
originalLegBackLeft.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/LegBackRight.obj"
originalLegBackRight.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/LeftArmrest.obj"
originalLeftArmrest.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/RightArmrest.obj"
originalRightArmrest.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/LeftBar.obj"
originalLeftBar.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairFancy2/meshes/RightBar.obj"
originalRightBar.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairBasic2/meshes/Back.obj"
anotherBack.DeepCopy(loadObj(stlFilename))

stlFilename = "last_examples/ChairBasic2/meshes/Back.obj"
anotherBack2.DeepCopy(loadObj(stlFilename))

originalBack_Bounds = originalBack.GetBounds()
anotherBack_Bounds = anotherBack.GetBounds()

print(abs(originalBack.GetBounds()[4] - originalBack.GetCenter()[2]))
print(abs(anotherBack.GetBounds()[4] - anotherBack.GetCenter()[2]))
print(anotherBack.GetCenter()[2])
final_distance = originalBack.GetCenter()[2] + (abs(originalBack.GetBounds()[4] - originalBack.GetCenter()[2]) - abs(anotherBack.GetBounds()[4] - anotherBack.GetCenter()[2]))
print(final_distance)
offset = final_distance - anotherBack.GetCenter()[2]
print(offset)
print(anotherBack.GetCenter())

transform = vtk.vtkTransform()
transform.Translate(0, 0, offset)
transformFilter = vtk.vtkTransformPolyDataFilter()
transformFilter.SetInputData(anotherBack)
transformFilter.SetTransform(transform)
transformFilter.Update()
anotherBack = transformFilter.GetOutput()

print(anotherBack.GetCenter())

# Append the two meshes 
appendFilter = vtk.vtkAppendPolyData()
if vtk.VTK_MAJOR_VERSION <= 5:
    appendFilter.AddInputConnection(originalBack.GetProducerPort())
    appendFilter.AddInputConnection(originalSeat.GetProducerPort())
    appendFilter.AddInputConnection(anotherBack.GetProducerPort())
else:
    #appendFilter.AddInputData(originalBack)
    appendFilter.AddInputData(originalSeat)
    appendFilter.AddInputData(originalLegBackLeft)
    appendFilter.AddInputData(originalLegBackRight)
    appendFilter.AddInputData(originalLegFrontLeft)
    appendFilter.AddInputData(originalLegFrontRight)
    appendFilter.AddInputData(originalLeftArmrest)
    appendFilter.AddInputData(originalRightArmrest)
    appendFilter.AddInputData(originalLeftBar)
    appendFilter.AddInputData(originalRightBar)

    appendFilter.AddInputData(anotherBack)
    #appendFilter.AddInputData(anotherBack2)

appendFilter.Update()

#  Remove any duplicate points.
cleanFilter = vtk.vtkCleanPolyData()
cleanFilter.SetInputConnection(appendFilter.GetOutputPort())
cleanFilter.Update()

# Create a mapper and originalBack
mapper = vtk.vtkPolyDataMapper()
mapper.SetInputConnection(cleanFilter.GetOutputPort())

actor = vtk.vtkActor()
actor.SetMapper(mapper)

# Create a renderer, render window, and interoriginalBack
renderer = vtk.vtkRenderer()
renderWindow = vtk.vtkRenderWindow()
renderWindow.AddRenderer(renderer)
renderWindowInterActor = vtk.vtkRenderWindowInteractor()
renderWindowInterActor.SetRenderWindow(renderWindow)

# Add the originalBacks to the scene
renderer.AddActor(actor)
renderer.SetBackground(.3, .2, .1) #  Background color dark red

# Render and interact
renderWindow.Render()
renderWindowInterActor.Start()
